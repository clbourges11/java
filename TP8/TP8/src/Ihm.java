import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class Ihm extends JFrame {
	private JTextField field = new JTextField("#Exemple");
	private JButton b = new JButton ("OK");
	private JPanel panel1 = new JPanel();
	private JPanel panel = new JPanel();
	private Font police = new Font("Gotham", Font.CENTER_BASELINE, 14);

	
	public Ihm() { 
		
		//Frame param
	    this.setTitle("Twitter IHM");
		this.setIconImage(new ImageIcon("index.png").getImage());
		this.setSize(1920,1080);
		
		//Field param
	    field.setFont(police);
	    field.setPreferredSize(new Dimension(150, 30));
	    field.setPreferredSize(new Dimension(150, 30));
	    
	    //Button param
	    b.setFont(police);
	    
	    //panel1 param
	  	panel1.setBackground(new Color(0,172,238));
	  		
	    //panel param
	  	panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));
		panel.setBackground(Color.WHITE);
	    b.addActionListener(new BoutonListener());
		panel1.add(field);
		panel1.add(b);

		this.getContentPane().add(panel1,BorderLayout.NORTH);
		this.setVisible(true);
		
	}
	
	// la fonction refresh est appelée par le bouton listener et permet de ré-actualiser après une nouvelle recherche
	public void refresh(){
		
		// On fait l'appel à Twitter4j dans un thread pour ne pas bloquer l'interface graphique
		MonRunnable run = new MonRunnable(field.getText());
		Thread t = new Thread(run);
		t.start();
		
		panel.removeAll(); // On profite de l'éxécution du thread pour nettoyer le panel de la requête précédente

		try {
			t.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		//Pour chaque tweet contenu dans la liste retournée par le thread, on créée un pannel
		for(int i=0;i<run.l.size();i++ )
		{
			JPanel pan = new JPanel();
			JTextArea lab = new JTextArea(run.l.get(i));
			lab.setSize(1500,30);
			lab.setLineWrap(true); 
			lab.setBorder(javax.swing.BorderFactory.createLineBorder(new Color(0,172,238)));
			lab.setFont(police);
			pan.add(lab);
			panel.add(pan);
		}
		this.getContentPane().add(panel);
		this.setVisible(true);
	}
	
	// BoutonListener détecte l'appui sur le bouton OK
	class BoutonListener implements ActionListener{
	    public void actionPerformed(ActionEvent e) {
	    	refresh();
	    }

	  }
		
}
