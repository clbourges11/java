import java.util.ArrayList;
import java.util.List;
import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.OAuth2Token;
import twitter4j.conf.ConfigurationBuilder;

public class Main {

	public static void main(String[] args) {
		try {
			
			// Connection
			OAuth2Token token = demander_token();
			ConfigurationBuilder cb = new ConfigurationBuilder();
			cb.setApplicationOnlyAuthEnabled(true);
			cb.setOAuthConsumerKey("hP49CWN0KuutGM6PCn7pfVk7P");
			cb.setOAuthConsumerSecret("vQXz8BZ1tKBiSx1YTRwlkzT0Tfqf72hTdTSHv59hw5T1WFc0V8");
			cb.setOAuth2TokenType(token.getTokenType());
			cb.setOAuth2AccessToken(token.getAccessToken());
			
			// on déclenche l'interface graphique
			new Ihm();			

		} catch (TwitterException e1) {
			e1.printStackTrace();
		}
	}
	
	// La fonction get_mot_cle retourne une liste de tweet à partir d'un mot clé
	public static List<String> get_mot_cle(String mot_cle) {
		List<String> l = new ArrayList<String>();
		try {
			
			// Connection
			OAuth2Token token = demander_token();
			ConfigurationBuilder cb = new ConfigurationBuilder();
			cb.setApplicationOnlyAuthEnabled(true);
			cb.setOAuthConsumerKey("hP49CWN0KuutGM6PCn7pfVk7P");
			cb.setOAuthConsumerSecret("vQXz8BZ1tKBiSx1YTRwlkzT0Tfqf72hTdTSHv59hw5T1WFc0V8");
			cb.setOAuth2TokenType(token.getTokenType());
			cb.setOAuth2AccessToken(token.getAccessToken());
			Twitter twitter = new TwitterFactory(cb.build()).getInstance();

			// Requête
			Query query = new Query(mot_cle);
		    QueryResult result;
			result = twitter.search(query);
			for (Status status : result.getTweets()) {
		        System.out.println("@" + status.getUser().getScreenName() + "\n\n" + status.getText()+"\n");
		        l.add(status.getUser().getScreenName() + ": \n \n" + status.getText());
		    }
		} catch (TwitterException e1) {
			e1.printStackTrace();
		}
	    return l;
	}
	
	// la fonction demander_token permet de récupérer le token
	public static OAuth2Token demander_token() throws TwitterException
	{
		ConfigurationBuilder cb = new ConfigurationBuilder();
		cb.setApplicationOnlyAuthEnabled(true)
        .setOAuthConsumerKey("hP49CWN0KuutGM6PCn7pfVk7P")
        .setOAuthConsumerSecret("vQXz8BZ1tKBiSx1YTRwlkzT0Tfqf72hTdTSHv59hw5T1WFc0V8");
		OAuth2Token token = new TwitterFactory(cb.build()).getInstance().getOAuth2Token();
		return token;
	}
}