#include<string>
class Etudiant
{
	private: std::string ine;
	private: std::string nom;
	private: std::string prenom;
	private: std::string date_de_naissance;

	public: std::string getIne();
	public: void setIne(std::string);
	public: std::string getNom();
	public: void setNom(std::string);
	public: std::string getPrenom();
	public: void setPrenom(std::string);
	public: std::string getDate_de_naissance();
	public: void setDate_de_naissance(std::string);
	public: void getAge();
	public: void afficher();
	public: std::string toString();
};
