import java.io.PrintWriter;
import java.lang.reflect.Modifier;

public class main {

	public static void main(String[] args) throws Exception {
		
		//Vérification des arguments en ligne de commande
		int stdout=0;
		int args_ok=0;;
		if(args.length == 0){
			System.out.println("Usage: nom_classe_source [nom_classe_destination] [--stdout]");
		}
		else if(args.length == 1 ){
			String[] temp = {args[0],args[0]};
			args=temp;
			args_ok=1;
		}
		else if(args.length == 2 ){
			if(args[1].compareTo("--stdout")==0){
				args[1]=args[0];
				stdout=1;
				args_ok=1;
			}
			else
			{
				args_ok=1;
			}
		}
		else if(args.length == 3){
			if(args[2].compareTo("--stdout")==0){
				stdout=1;
				args_ok=1;
			}
			else{
				System.out.println("Usage: nom_classe_source [nom_classe_destination] [--stdout]");
			}
		}
		else
		{
			System.out.println("Usage: nom_classe_source [nom_classe_destination] [--stdout]");

		}
		try{
		Class test = Class.forName(args[0]);
		}
		catch(Exception e)
		{
			System.out.println("Classe non trouvée, vérifiez le classpath");
		}
		
		System.out.println(Class.forName(args[0]).getDeclaredConstructors().length);
		// Création du hpp

		if(args_ok==1){
			
	
			// On nomme la classe
			if (stdout ==1 )System.out.println(args[1]+".hpp  :");
	
			PrintWriter writer = new PrintWriter(args[1]+".hpp", "UTF-8");
			writer.println("#include<string>");
			writer.print("class ");
			writer.println(args[1]);
			writer.println("{");
			
			if (stdout ==1 )System.out.println("#include<string>");
			if (stdout ==1 )System.out.print("class ");
			if (stdout ==1 )System.out.println(args[1]);
			if (stdout ==1 )System.out.println("{");
			
			// On traite les attributs
			for(int i=0; i<Class.forName(args[0]).getDeclaredFields().length;i++){
				int j = Class.forName(args[0]).getDeclaredFields()[i].getModifiers();
			    String retval = Modifier.toString(j);
			    String name = Class.forName(args[0]).getDeclaredFields()[i].getName();
			    String type = Class.forName(args[0]).getDeclaredFields()[i].getType().getSimpleName();
			    if(type.compareTo("String")==0)type="std::string";
			    if(type.compareTo("Date")==0)type="std::string";
			    writer.println("	"+retval+": "+type+" "+name+";");
				if (stdout ==1 )System.out.println("	"+retval+": "+type+" "+name+";");
			}
			
			writer.println("");
			
			// On traite les constructeurs
			if (stdout ==1 )System.out.println("");
			for(int a=0; a<Class.forName(args[0]).getDeclaredConstructors().length;a++){
			    writer.print("	"+retval_cons+":"+"(");
			    if (stdout ==1 )System.out.print("	"+retval_cons+":"+name_cons+"(");
			 // On traite les paramètres des constructeurs
			    if(Class.forName(args[0]).getDeclaredConstructors()[a].getParameterCount()>0)
			    {
			    	String type_parameter=Class.forName(args[0]).getDeclaredConstructors()[a].getParameters()[0].getType().getSimpleName();
				    if(type_parameter.compareTo("String")==0)type_parameter="std::string";
				    if(type_parameter.compareTo("Date")==0)type_parameter="std::string";
	
			    	writer.print(type_parameter);
			    	if (stdout ==1 )System.out.print(type_parameter);
			    	
				    for(int k=1;k<Class.forName(args[0]).getDeclaredMethods()[a].getParameterCount();k++){
				    	String type_parameter2=Class.forName(args[0]).getDeclaredMethods()[k].getParameters()[0].getType().getSimpleName();
					    if(type_parameter2.compareTo("String")==0)type_parameter2="std::string";
					    if(type_parameter2.compareTo("Date")==0)type_parameter2="std::string";
				    	writer.print(","+type_parameter2);
				    	if (stdout ==1 )System.out.print(","+type_parameter2);
				    }
				}
			    writer.println(");");
			    if (stdout ==1 )System.out.println(");");
			    
			}
			    
			// On traite les méthodes
			for(int i=0; i<Class.forName(args[0]).getDeclaredMethods().length;i++){
				int j = Class.forName(args[0]).getDeclaredMethods()[i].getModifiers();
			    String retval = Modifier.toString(j);
			    String type_retour = Class.forName(args[0]).getDeclaredMethods()[i].getReturnType().getSimpleName();
			    if(type_retour.compareTo("String")==0)type_retour="std::string";
			    if(type_retour.compareTo("Date")==0)type_retour="std::string";
	
			    String name = Class.forName(args[0]).getDeclaredMethods()[i].getName();
	
			    writer.print("	"+retval+": "+type_retour+" "+name+"(");
			    if (stdout ==1 )System.out.print("	"+retval+": "+type_retour+" "+name+"(");
			    
			    // On traite les paramètres des méthodes
			    if(Class.forName(args[0]).getDeclaredMethods()[i].getParameterCount()>0)
			    {
			    	String type_parameter=Class.forName(args[0]).getDeclaredMethods()[i].getParameters()[0].getType().getSimpleName();
				    if(type_parameter.compareTo("String")==0)type_parameter="std::string";
				    if(type_parameter.compareTo("Date")==0)type_parameter="std::string";
	
			    	writer.print(type_parameter);
			    	if (stdout ==1 )System.out.print(type_parameter);
			    	
				    for(int k=1;k<Class.forName(args[0]).getDeclaredMethods()[i].getParameterCount();k++){
				    	String type_parameter2=Class.forName(args[0]).getDeclaredMethods()[k].getParameters()[0].getType().getSimpleName();
					    if(type_parameter2.compareTo("String")==0)type_parameter2="std::string";
					    if(type_parameter2.compareTo("Date")==0)type_parameter2="std::string";
				    	writer.print(","+type_parameter2);
				    	if (stdout ==1 )System.out.print(","+type_parameter2);
				    }
				}
			    writer.println(");");
			    if (stdout ==1 )System.out.println(");");
	
			}
			writer.println("};");
			if (stdout ==1 )System.out.println("};");
			writer.close();
			
			
			
			
			// Création du .cpp
			
			if (stdout ==1 )System.out.println(args[1]+".cpp  :");
	
			PrintWriter writer2 = new PrintWriter(args[1]+".cpp", "UTF-8");
			writer2.print("#include \"");
			writer2.print(args[1]);
			writer2.println(".hpp\"");
			
			if (stdout ==1 )System.out.print("#include\"");
			if (stdout ==1 )System.out.print(args[1]);
			if (stdout ==1 )System.out.println(".hpp\"");
			
			// On traite les méthodes
			for(int i=0; i<Class.forName(args[0]).getDeclaredMethods().length;i++){
			    String type_retour = Class.forName(args[0]).getDeclaredMethods()[i].getReturnType().getSimpleName();
			    if(type_retour.compareTo("String")==0)type_retour="std::string";
			    if(type_retour.compareTo("Date")==0)type_retour="std::string";
	
			    String name = Class.forName(args[0]).getDeclaredMethods()[i].getName();
	
			    writer2.print(type_retour+" "+args[1]+"::"+name+"(");
			    if (stdout ==1 )System.out.print(type_retour+" "+args[1]+"::"+name+"(");
			    
			    // On traite les paramètres des méthodes
			    if(Etudiant.class.getDeclaredMethods()[i].getParameterCount()>0)
			    {
			    	String type_parameter=Class.forName(args[0]).getDeclaredMethods()[i].getParameters()[0].getType().getSimpleName();
				    if(type_parameter.compareTo("String")==0)type_parameter="std::string";
				    if(type_parameter.compareTo("Date")==0)type_parameter="std::string";
	
			    	writer2.print(type_parameter);
			    	if (stdout ==1 )System.out.print(type_parameter);
			    	
				    for(int k=1;k<Class.forName(args[0]).getDeclaredMethods()[i].getParameterCount();k++){
				    	String type_parameter2=Class.forName(args[0]).getDeclaredMethods()[k].getParameters()[0].getType().getSimpleName();
					    if(type_parameter2.compareTo("String")==0)type_parameter2="std::string";
					    if(type_parameter2.compareTo("Date")==0)type_parameter2="std::string";
				    	writer2.print(","+type_parameter2);
				    	if (stdout ==1 )System.out.print(","+type_parameter2);
				    }
				}
			    writer2.println(")");
			    if (stdout ==1 )System.out.println(")");
			    writer2.println("{");
			    if (stdout ==1 )System.out.println("{");
			    writer2.println("");
			    if (stdout ==1 )System.out.println("");
			    writer2.println("}");
			    if (stdout ==1 )System.out.println("}");
	
			}
			writer2.close();
		}
	}

}
