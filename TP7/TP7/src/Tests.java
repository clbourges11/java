import java.io.File;
import java.util.Date;

import junit.framework.TestCase;

public class Tests extends TestCase{
	
	
	// On teste sur 20 éxecutions la rapidité de notre générateur séquentiel
	public void testExecution_Classique() {
		int somme=0;
		for(int k=0;k<20;k++)
		{
			Date d = new Date();
			long time1 = d.getTime();
			assertTrue(Generateur.Generateur_simple().size()==150000);
	
			Date d2 = new Date();
			long time2 = d2.getTime();
			//System.out.println("Générateur simple: "+(time2-time1));			
			somme+=(time2-time1);
		}
		System.out.println("Moyenne sur 20 éxécutions pour le generateur séquentiel: "+somme/20);

	}
	
	
	// On teste sur 20 éxecutions la rapidité de notre générateur threadé
	synchronized public void testExecution_Threadee() {
		
		int somme=0;
		for(int k=0;k<20;k++)
		{
			Date d = new Date();
			long time1 = d.getTime();
			assertTrue(Generateur.Generateur_thread(10).size()==150000);
			Date d2 = new Date();
			long time2 = d2.getTime();
			//System.out.println("Générateur config: "+(time2-time1));
			somme+=(time2-time1);
			
		}
		System.out.println("Moyenne sur 20 éxécutions pour le generateur thread: "+somme/20);

	}
	
	// On génère les éutdiants et on les ajoute au fur et à mesure dans un fichier
	synchronized public void testEcriture_Sequentielle() {
			int somme=0;
			for(int k=0;k<10;k++)
			{
				File f = new File("Etudiant_seq");
				f.delete();
				Date d = new Date();
				long time1 = d.getTime();
				Fichier.Ecriture_sequentielle_fichier();
				Date d2 = new Date();
				long time2 = d2.getTime();
				somme+=(time2-time1);
				System.out.print(".");
				
			}
			System.out.println("Moyenne sur 10 éxécutions pour le generateur avec ecriture séquentielle dans un fichier :"+somme/10);
	
		}
	
	//
	synchronized public void testEcriture_Thread() {
		int somme=0;
		for(int k=0;k<10;k++)
		{
			Date d = new Date();
			long time1 = d.getTime();
			Fichier.Ecriture_thread_fichier(3);
			Date d2 = new Date();
			long time2 = d2.getTime();
			somme+=(time2-time1);
			System.out.print(".");
			
		}
		System.out.println("Moyenne sur 10 éxécutions pour le generateur avec ecriture dans fichier avec thread :"+somme/10);

	}
	

}
