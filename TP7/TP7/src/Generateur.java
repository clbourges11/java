import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


// la classe Generateur implémente deux méthodes permettant de créer des List d'objets étudiants
public class Generateur {

	
	//Le générateur simple créer 150000 étudaint de façon séquentielle
	public static List<Etudiant> Generateur_simple() {
		List<Etudiant> l2 = new ArrayList<Etudiant>();
		for(int k=0; k<150000;k++)
		{
			l2.add(new Etudiant());
		}
		return l2;
	}
	
	//Le générateur config permet de créer 150000 étudiants avec n thread 
	synchronized public static List<Etudiant> Generateur_thread (int n) {
		List<Etudiant> l2 = Collections.synchronizedList(new ArrayList<Etudiant>());
		Monrunnable r = new Monrunnable(l2,n);
		List<Thread> threads = new ArrayList<Thread>();
		for(int k=0;k<n;k++){
			threads.add(new Thread(r));
		}
		for(int k=0;k<n;k++){
			threads.get(k).start();
		}
		try {
			for(int k=0;k<n;k++){
				threads.get(k).join();
				//System.out.println("On attend le thread"+k);
			}

		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		//System.out.println("On renvoie");
		return l2;
	}

	
}
