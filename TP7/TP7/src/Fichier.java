import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.List;


// La classe Fichier implémente les fonctions de génération qui stockent les objets dans un fichier

public class Fichier {
	
	// On créée 150000 objets étudiants et on les stocke de façon séquentielle dans un fichier
	synchronized public static void Ecriture_sequentielle_fichier () {
		File f = new File("./Etudiant_seq");
		try
		{
			f.createNewFile();
		    ObjectOutputStream oos = new ObjectOutputStream (new FileOutputStream (f));
		    for(int i=0;i<150000;i++)
		    {
		    	oos.writeObject (new Etudiant());
		    }
		    oos.close();
		}
		
		catch (IOException exception)
		{
		    System.out.println ("Erreur lors de l'écriture : " + exception.getMessage());
		}
	}
	
	
	
	// Chaque thread créée les objets étudiants, les stocke dans un fichier, puis les fichiers créés sont rassemblés
	synchronized public static void Ecriture_thread_fichier (int n) {
		File f = new File("Etudiant_thread");
		f.delete();
		// Chaque runnable ecrira dans un fichier différent
		List<Monrunnable_fichier> r = new ArrayList<Monrunnable_fichier>();
		for(int k=0;k<n;k++){
			r.add(new Monrunnable_fichier(n,k));
		}
		
		List<Thread> threads = new ArrayList<Thread>();
		for(int k=0;k<n;k++){
			threads.add(new Thread(r.get(k)));
		}
		for(int k=0;k<n;k++){
			threads.get(k).start();
		}
		
		// On attend la fin de tous les threads
		try {
			for(int k=0;k<n;k++){
				threads.get(k).join();
				//System.out.println("On attend le thread"+k);
			}
			
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		for(int k=0;k<n;k++){
			try {
				copier("Etudiant"+k,"Etudiant_thread");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}
	
	
	//Fonction qui permet de copier un fichier vers un autre
	private static void copier(String fichier_source, String fichier_dest)throws IOException
    {
		FileInputStream src = new FileInputStream(fichier_source);
	    FileOutputStream dest = new FileOutputStream(fichier_dest,true);
	 
	    FileChannel inChannel = src.getChannel();
	    FileChannel outChannel = dest.getChannel();
	 
	    for (ByteBuffer buffer = ByteBuffer.allocate(1024*1024);
	         inChannel.read(buffer) != -1;
	         buffer.clear()) {
	       buffer.flip();
	       while (buffer.hasRemaining()) outChannel.write(buffer);
	    }
	 
	    inChannel.close();
	    outChannel.close();
	    src.close();
	    dest.close();
    }
}
