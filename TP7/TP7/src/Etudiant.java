import java.util.Date;



public class Etudiant implements java.io.Serializable{
	
		private double ine;
		private String nom;
		private String prenom;
		private Date date_de_naissance;
		private String lieu_de_naissance;
		private String descriptif_parcours;
		
		public Etudiant() {
			int compteur=1;
			//Permet de ralentir la création d'étudiants
			for(int k=0;k<1000000000;k++)
			{
				compteur*=k;
			}
			this.ine =Math.random()*compteur;
			this.nom = "nom";
			this.prenom = "prenom";
			this.date_de_naissance = new Date(2018-10-10);
			this.lieu_de_naissance="lieu_de_naissance";
			this.descriptif_parcours="descriptif_parcours";
		}
		
}
