import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

// La classe Monrunnable_fichier permettra de créer d'avoir un "run" qui écrira dans un fichier
public class Monrunnable_fichier implements Runnable {
	public File fichier;
	public int nb_thread;
	
	// On créée le runnable avec la liste d'étudiant à remplir et le nombre de thread voulu
	public Monrunnable_fichier(int n, int m)
	{
		fichier = new File ("./Etudiant"+m);
		nb_thread=n;		
	}
	synchronized public void run(){
		
		try {
			ObjectOutputStream oos = new ObjectOutputStream (new FileOutputStream (fichier));
			for(int k=0; k<150000/nb_thread;k++)
			{
			    oos.writeObject (new Etudiant());
			}
		    oos.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

	}
}
