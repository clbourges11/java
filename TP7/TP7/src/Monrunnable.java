import java.util.List;

//La classe monrunnable implémente la fonction "run" qui permet de créer les objets étudiants
public class Monrunnable implements Runnable{
	
	public List<Etudiant> l;
	int nb_thread;
	
	// On créée le runnable avec la liste d'étudiant à remplir et le nombre de thread voulu
	public Monrunnable(List<Etudiant> l1, int n)
	{
		l=l1;
		nb_thread=n;
	}
	
	// La méthode run s'adapte au nombre de threads pour créer le bon nombre d'étudiants et en avoir 150000 à la fin
	synchronized public void run(){
		for(int k=0; k<150000/nb_thread;k++)
		{
			l.add(new Etudiant());
		}
	}

}
