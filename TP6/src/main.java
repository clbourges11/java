import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Date;
import java.util.Scanner;
import com.google.gson.Gson;


public class main{

	public static void main(String [] args){
		Date date = new Date();
		System.out.println("1) Cr�er ou r�initialiser la table\n2) Consulter OpenWeather\n3) Lister les donn�es en base\n4) Nettoyer la base");
		Scanner sc = new Scanner(System.in);
		int choix = sc.nextInt();
		
		//Cr�ation de la base de donn�es
		if(choix==1) {
			File fichier = new File("test.db");
			if(fichier.delete())
			{
				System.out.println("L'ancienne base de donn�es est supprim�e");
			}
		
			//On cr��e la base
			String url = "jdbc:sqlite:./test.db";
	        try(
	        	Connection conn = DriverManager.getConnection(url);
	        		)
	        {
	            	
	                //On cr��e la table
			        String sql = "CREATE TABLE IF NOT EXISTS villes (\n"
			                + "	nom text PRIMARY KEY,\n"
			                + "	temp num,\n"
			                + "	description text,\n"
			                + "	pressure num,\n"
			                + "	humidity num,\n"
			                + "	temp_min num,\n"
			                + "	temp_max num,\n"
			                + "expir\n"
			                + ");";
			        Statement stmt = conn.createStatement();	            
			        stmt.execute(sql);
	                System.out.println("Cr�ation de la base de donn�es");
		            stmt.close();

	          }
	        catch (SQLException e) {
	            System.out.println(e.getMessage());
	        }
		}
	    
		
		// Nouvelle requ�te m�t�o
	    else if (choix==2) {
	    	System.out.println("Nom de la ville ?");
	    	Scanner sc2 = new Scanner(System.in);
			String ville = sc2.nextLine();
			sc2.close();
			
			// On interroge OpenWeather
			HttpURLConnection urlConnection = null;
			try {
				URL url = new URL("http://api.openweathermap.org/data/2.5/weather?q="+ville+"&APPID=c62ff9852f25d0252f6681f2d0205609&units=metric");
				urlConnection = (HttpURLConnection) url.openConnection();
				InputStream in = new BufferedInputStream(urlConnection.getInputStream());
				Reader r  = new InputStreamReader(in, "UTF-8");			
				Gson gson= new Gson();
				Ville v = gson.fromJson(r, Ville.class);
				System.out.println(v.toString());
				sc2.close();
				
				// On ajoute la donn�e en base
				String url2 = "jdbc:sqlite:./test.db";
	        	try(Connection conn = DriverManager.getConnection(url2);)
	        	{
		            String insert = "INSERT INTO villes(nom,temp,description,pressure,humidity,temp_min,temp_max,expir) VALUES(?,?,?,?,?,?,?,?)";            PreparedStatement pstmt = conn.prepareStatement(insert);
		            pstmt.setString(1, v.name);
		            pstmt.setDouble(2, v.main.temp);
		            pstmt.setString(3, v.weather.get(0).description);
		            pstmt.setFloat(4, v.main.pressure);
		            pstmt.setInt(5, v.main.humidity);
		            pstmt.setDouble(6, v.main.temp_min);
		            pstmt.setDouble(7, v.main.temp_max);
		            pstmt.setDouble(7, v.main.temp_max);
		            pstmt.setDouble(8, date.getTime());
		            pstmt.executeUpdate();
	                System.out.println("Insertion dans la base effectu�e");
				}
	        	catch (Exception e){
	        		System.out.println("Donn�es non ins�r�es en base (car peut-�tre d�j� existantes )");	        	}
			}
			catch (Exception e){
						System.out.println(e.getMessage());
				}
			
			}
		
		
			//Afficher la base de donn�es
		    else if (choix==3) {
		    	System.out.println("1) Trier les villes par ordre alphab�tique\n2) Trier par temp�rature");
				Scanner sc3 = new Scanner(System.in);
				int tri = sc3.nextInt();
				sc3.close();
		    	lister(tri);
		    	
		    }
		
		// Nettoyage de la base de donn�es
		// Lors d'un nettoyage les donn�es qui ont plus de 24h sont supprim�es
		else if (choix==4){
			double limite = date.getTime()-(double)86400000; // 86400000 ms <-> 24h
			String req = "DELETE from villes WHERE expir <"+Double.toString(limite)+";";
			String url3 = "jdbc:sqlite:./test.db";
	    	try(Connection conn = DriverManager.getConnection(url3);
	    			Statement stmt = conn.createStatement();
	    			)
	    	{
	    		stmt.executeUpdate(req);
	    	}
	    	catch (SQLException e) {
	            System.out.println(e.getMessage());
	        }
		}
		
		else
		{
			System.out.println("Veuillez choisir un chiffre entre 1 et 4");
		}
		sc.close();

	}
	
	
	// Fonction pour lister les donn�es en base en fonction du tri choisi
	static void lister (int tri){ 
		
		String req="";
		if(tri==1 || tri==2) 
		{
			if(tri==1)
			{
			req = "SELECT * FROM villes ORDER BY nom";
			 }
			else if (tri==2) {
				req = "SELECT * FROM villes ORDER BY temp";
			}
			String url3 = "jdbc:sqlite:./test.db";
		   	try(Connection conn = DriverManager.getConnection(url3);
		   			Statement stmt = conn.createStatement();
		   				ResultSet rs    = stmt.executeQuery(req);
		   			)
		   	{
	           while (rs.next()) {
	               System.out.println("Nom: "+rs.getString("nom")+"  Temp�rature: "+rs.getString("temp")+"  Temps: "+rs.getString("description")+"  Pression: "+rs.getString("pressure")+"   Humidit�: "+rs.getString("humidity")+"  Temp�rature minimale: "+rs.getString("temp_min")+"   Temp�rature maximale: "+rs.getString("temp_max"));
	              
	           }
		   	}
		   	catch (SQLException e) {
		           System.out.println(e.getMessage());
		    }
		}
		else
		{
			System.out.println("Veuillez choisir un chiffre entre 1 et 2");
		}
		       
	   	
   }
	
}






