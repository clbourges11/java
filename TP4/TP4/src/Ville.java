import java.util.List;

public class Ville {
	Coord coord;
	List<Weather> weather;
	Meteo main;
	String name;
	public String toString() {
		return String.format("Nom: %s \nLongitude: %f\nLatitude: %f \nTemps:%s \nT:%f°C  \nTempérature minimale:%f°C \nTempérature maximale:%f°C \nPression: %d \nHumidité: %d",name, coord.lon, coord.lat, weather.get(0).description,main.temp,main.temp_min,main.temp_max,main.pressure,main.humidity);
	}
}
