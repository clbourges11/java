import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;
import com.google.gson.Gson;

public class main{

	public static void main(String [] args){
		System.out.println("Nom de la ville ?");
		Scanner sc = new Scanner(System.in);
		String ville = sc.nextLine();
		HttpURLConnection urlConnection = null;
		try {
			URL url = new URL("http://api.openweathermap.org/data/2.5/weather?q="+ville+"&APPID=c62ff9852f25d0252f6681f2d0205609&units=metric");
			urlConnection = (HttpURLConnection) url.openConnection();
			
			InputStream in = new BufferedInputStream(urlConnection.getInputStream());
			Reader r  = new InputStreamReader(in, "UTF-8");			
			Gson gson= new Gson();
			Ville v = gson.fromJson(r, Ville.class);
			System.out.println(v.toString());
			} catch (Exception e){
			if(urlConnection != null){
				urlConnection.disconnect();
				System.out.println("Ville non trouvée, vérifiez l'orthographe.");
			}
		}

	}

}
