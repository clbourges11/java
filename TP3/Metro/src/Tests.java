import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import junit.framework.TestCase;

public class Tests extends TestCase {
	public void testOuvertureFichier() throws Exception{
		Parser p = new Parser();
		assertTrue(p.fileToListOfString("../ratp_arret.csv").size()>0);
	}
	public void testParseur_Format_Entree(){ // On vérifie qu'une entrée au mauvais format n'est pas prise en compte
		List<String> test = new ArrayList<String>();
		test.add("monid#2.30088336611718#48.8646460306735#Alma-Marceau#PARIS-16EME#metro");
		List<MetroStop> m;
		Parser p = new Parser();
		m=p.parse(test);
		assertTrue(m.size()==0);
	}
	public void testParseur_Nombre_Entree(){ // On vérifie qu'une entrée qui n'a pas suffisament d'arguments n'est pas prise en compte
		List<String> test = new ArrayList<String>();
		test.add("2#2.30088336611718#48.8646460306735#Alma-Marceau#PARIS-16EME#metro");
		test.add("11462747#2.32561963015676#");
		List<MetroStop> m;
		Parser p = new Parser();
		m=p.parse(test);
		assertTrue(m.size()==1);
	}
	public void testParseur_Seulement_Metro(){
		List<String> test = new ArrayList<String>();
		test.add("1980#2.30088336611718#48.8646460306735#Alma-Marceau#PARIS-16EME#metro");
		test.add("11462747#2.32561963015676#48.9328393085378#ACACIAS#VILLENEUVE-LA-GARENNE#bus");
		List<MetroStop> m;
		Parser p = new Parser();
		m=p.parse(test);
		assertTrue(m.size()==1);
	}
	public void testParseur_Attributs(){
		List<String> test = new ArrayList<String>();
		test.add("1980#2.30088336611718#48.8646460306735#Alma-Marceau#PARIS-16EME#metro");
		List<MetroStop> m;
		Parser p = new Parser();
		m=p.parse(test);
		assertTrue(m.get(0).id==1980);
		assertTrue(m.get(0).lat==2.30088336611718);
		assertTrue(m.get(0).lgt==48.8646460306735);
		assertTrue(m.get(0).nom.contentEquals("Alma-Marceau"));
		assertTrue(m.get(0).ville.contentEquals("PARIS-16EME"));
		assertTrue(m.get(0).type.contentEquals("metro"));
	}
	public void testTri_ID(){
		List<String> test = new ArrayList<String>();
		test.add("1980#2.30088336611718#48.8646460306735#Alma-Marceau#PARIS-16EME#metro");
		test.add("1970#2.30088336611718#48.8646460306735#Alma-Marceau#PARIS-16EME#metro");
		List<MetroStop> m;
		Parser p = new Parser();
		m=p.parse(test);
		assertTrue(m.get(0).id==1980);
		assertTrue(m.get(1).id==1970);
		Collections.sort(m, MetroStop.IDComparator);
		assertTrue(m.get(0).id==1970);
		assertTrue(m.get(1).id==1980);
	}
	public void testTri_Arrond(){
		List<String> test = new ArrayList<String>();
		test.add("1#2.40638582344912#48.8770699912197#Porte des Lilas#PARIS-20EME#metro");
		test.add("2#2.30088336611718#48.8646460306735#Alma-Marceau#PARIS-16EME#metro");
		List<MetroStop> m;
		Parser p = new Parser();
		m=p.parse(test);
		assertTrue(m.get(0).id==1);
		assertTrue(m.get(1).id==2);
		Collections.sort(m, MetroStop.ArrondPuisNomComparator);
		assertTrue(m.get(0).id==2);
		assertTrue(m.get(1).id==1);
	}
	public void testTri_ArrondPuisNom(){
		List<String> test = new ArrayList<String>();
		test.add("1#2.40638582344912#48.8770699912197#Porte des Lilas#PARIS-20EME#metro");
		test.add("2#2.40135199013901#48.868519874537#Pelleport#PARIS-20EME#metro");

		List<MetroStop> m;
		Parser p = new Parser();
		m=p.parse(test);
		assertTrue(m.get(0).id==1);
		assertTrue(m.get(1).id==2);
		Collections.sort(m, MetroStop.ArrondPuisNomComparator);
		assertTrue(m.get(0).id==2);
		assertTrue(m.get(1).id==1);
	}
}
