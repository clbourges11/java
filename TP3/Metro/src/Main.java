import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class Main {
	public static void main(String[] args) throws Exception {
		int continuer=1;
		Parser p = new Parser();
		List<String> arrets = p.fileToListOfString("../ratp_arret.csv");
		List<MetroStop>metros=p.parse(arrets);
		Scanner sc = new Scanner(System.in);

		while(continuer == 1)
		{

			//Menu
			System.out.println("1) Afficher liste\n2) Trier liste ID\n3) Trier liste arrondissment puis par nom de station\n");
			System.out.println("Veuillez saisir votre choix :");
			int choix = sc.nextInt();
			sc.nextLine();

			//Afficher liste
			if(choix == 1){
				
				for (int k=0;k<metros.size();k++)
				{
					MetroStop m = metros.get(k);
					System.out.println(m.id+" "+m.lat+" "+m.lgt+" "+m.nom+" "+m.type+" "+m.ville + " "+m.nom);
				}
			}
			//Trier par ID
			else if(choix == 2){
				Collections.sort(metros, MetroStop.IDComparator);

			}
			//Trier par arrondissement puis par nom de station
			else if(choix == 3){
				Collections.sort(metros, MetroStop.ArrondPuisNomComparator);

			}
		}
		sc.close();
		
	}
}
