import java.io.BufferedReader;
import java.io.FileReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

public class Parser {
	public List<String> fileToListOfString (String pat) throws Exception { // Passage d'un fichier à une List<String>, un élément = une ligne
		Reader reader = new FileReader(pat);
		BufferedReader br = new BufferedReader(reader);
		List<String> l = new ArrayList<String>();
		String line;
		while((line = br.readLine())!=null){
			l.add(line);
		}
		br.close();
		return l;
	}
	public static boolean isInt(String str)  
	{  
	  try  
	  {  
	    int n = Integer.parseInt(str);  
	  }  
	  catch(NumberFormatException nfe)  
	  {  
	    return false;  
	  }  
	  return true;  
	}
	public static boolean isNumeric(String str)  
	{  
	  try  
	  {  
	    double d = Double.parseDouble(str);  
	  }  
	  catch(NumberFormatException nfe)  
	  {  
	    return false;  
	  }  
	  return true;  
	}
	
	public List<MetroStop> parse (List<String> lines)  // Passage d'une List<String> à une List<MetroStop> en filtrant pour enlever bus,tram,etc..
	{
		List<MetroStop> l = new ArrayList<MetroStop>();
		for(int i=0;i<lines.size();i++){
			String line=(String) lines.get(i);
			String [] parts = line.split("#");
			if(parts.length == 6) // On vérifie que la ligne dispose bien de 6 arguments sinon l'entrée n'est pas prise en compte
			{
				if(parts[5].compareTo("metro")==0) // On ne récupère que les stations de métro
				{
					if(isInt(parts[0]) && isNumeric(parts[1]) && isNumeric(parts[2]))
					{
						MetroStop met = new MetroStop();
						met.id=Integer.parseInt(parts[0]);
						met.lat=Double.parseDouble(parts[1]);
						met.lgt=Double.parseDouble(parts[2]);
						met.nom=parts[3];
						met.ville=parts[4];
						met.type=parts[5];
						l.add(met);
					}
				}
			}
		}
		return l;
	}
}
