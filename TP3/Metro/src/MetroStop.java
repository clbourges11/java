import java.util.Comparator;

public class MetroStop {
	int id;
	public double lat;
	public double lgt;
	public String nom;
	public String ville;
	public String type;
	
	public static Comparator<MetroStop> IDComparator = new Comparator<MetroStop>() { //trier par ID
		public int compare(MetroStop s1, MetroStop s2) {
			 int ID1 = s1.id;
			 int ID2 = s2.id;
			 return ID1-ID2;
			}
	};
	public static Comparator<MetroStop> ArrondPuisNomComparator = new Comparator<MetroStop>() {
		public int compare(MetroStop s1, MetroStop s2) {
			 String ville1 = s1.ville;
			 String ville2 = s2.ville;
			 int comp =0;
			 if(ville1.compareTo(ville2)==0) // Si même arrondissement, trier par nom de station
			 {
				 String nom1 = s1.nom;
				 String nom2 = s2.nom;
				 comp = nom1.compareTo(nom2);
			 }
			 else						// Si arrondissement différents, trier par arrondissement
			 {
				 comp=ville1.compareTo(ville2);
			 }
			 return comp;
			}
	};
}
