import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.Collections;

public class Main{
	public static void main (String[] argv){

			List<Document> list = new ArrayList<Document>();
			int continuer=1;
			//Ajout automatique
			Document l2 = new Livre("montitre2", "monprix2","2018-06-24","macategorie2","6");
			list.add(l2);
			Document l3 = new Livre("montitre3", "monprix3","2015-01-19","macategorie3","2");
			list.add(l3);
			Document l1 = new Livre("montitre", "monprix","1996-10-22","macategorie","8");
			list.add(l1);
			Document l4 = new Magazine("montitre4", "monprix4","1969-12-18","macategorie4","periodicite4","9");
			list.add(l4);
			Document l5 = new Magazine("montitre5", "monprix5","2014-08-08","macategorie5","periodicite5","9.4");
			list.add(l5);

			while(continuer == 1)
			{

				//Menu
				System.out.println("1) Ajouter livre\n2) Ajouter magazine\n\n3) Afficher la liste\n4) Afficher la liste des livres\n5) Afficher la liste des magazines\n\n6) Trier par ordre alphabétique\n7) Trier par date de sortie\n8) Trier par note\n\n9)Quitter\n");
				Scanner sc = new Scanner(System.in);
				System.out.println("Veuillez saisir votre choix :");
				int choix = sc.nextInt();
				sc.nextLine();



				//Actions

				//Ajouter Livre
				if(choix == 1){
					System.out.println("Titre?");
					String titre = sc.nextLine();
					System.out.println("Prix?");
					String prix = sc.nextLine();
					System.out.println("Date?  (Veuillez respecter le format AAAA-MM-JJ)");
					String date = sc.nextLine();
					System.out.println("Catégorie?");
					String categorie = sc.nextLine();
					System.out.println("Note?");
					String note = sc.nextLine();
					Document nouveau = new Livre(titre, prix,date,categorie,note);
					list.add(nouveau);
				}

				//Ajouter magazine
				else if(choix == 2){
					System.out.println("Titre?");
					String titre = sc.nextLine();
					System.out.println("Prix?");
					String prix = sc.nextLine();
					System.out.println("Date?  (Veuillez respecter le format AAAA-MM-JJ)");
					String date = sc.nextLine();
					System.out.println("Catégorie?");
					String categorie = sc.nextLine();
					System.out.println("Périodicité?");
					String periodicite = sc.nextLine();
					System.out.println("Note?");
					String note = sc.nextLine();
					Document nouveau = new Magazine(titre, prix,date,categorie,periodicite,note);
					list.add(nouveau);
				}






				// Afficher liste
				else if (choix == 3) {
					for(int i = 0; i<list.size(); i++)
					{
						if(list.get(i).getClass().getName()=="Livre"){
							Livre liv=(Livre)list.get(i);
					  	System.out.println("Livre:  "+liv.titre+"  "+liv.prix+"  "+liv.date+"  "+liv.categorie+"  "+liv.note);
						}
						else{
							Magazine mag=(Magazine)list.get(i);
							System.out.println("Magazine:  "+mag.titre+"  "+mag.prix+"  "+mag.date+"  "+mag.categorie+"  "+mag.periodicite+"  "+mag.note);
						}
					}
					System.out.println("\n");
					sc.nextLine(); //On ne réaffiche pas tout de suite le menu
				}


				// Afficher liste des livres
				else if (choix == 4) {
					for(int i = 0; i<list.size(); i++)
					{
						if(list.get(i).getClass().getName()=="Livre"){
							Livre liv=(Livre)list.get(i);
					  	System.out.println("Livre:  "+liv.titre+"  "+liv.prix+"  "+liv.date+"  "+liv.categorie+"  "+liv.note);
						}
					}
					System.out.println("\n");
					sc.nextLine(); //On ne réaffiche pas tout de suite le menu

				}


				//Afficher liste des magazines
				else if (choix == 5) {
					for(int i = 0; i<list.size(); i++)
					{
						if(list.get(i).getClass().getName()=="Magazine"){
							Magazine mag=(Magazine)list.get(i);
							System.out.println("Magazine:  "+mag.titre+"  "+mag.prix+"  "+mag.date+"  "+mag.categorie+"  "+mag.periodicite+"  "+mag.note);
						}
					}
					System.out.println("\n");
					sc.nextLine(); //On ne réaffiche pas tout de suite le menu

				}





				//Trier ordre alphabétique
				else if (choix == 6) {
	   			Collections.sort(list, Document.TitreComparator);
				}

				//Trier date de sortie
				else if (choix == 7) {
					Collections.sort(list, Document.DateComparator);
				}

				//Trier note
				else if (choix == 8) {
					Collections.sort(list, Document.NoteComparator);
				}


				//Quitter
				else if (choix == 9) {
					continuer=0;
				}
				else{
					System.out.println("Veuillez saisir un chiffre entre 1 et 3");
				}
		}
	}
}
