import java.util.Comparator;

public class Document{
	protected String titre;
	protected String prix;
	protected String date;
	protected String categorie;
	protected String note;

	public static Comparator<Document> TitreComparator = new Comparator<Document>() {
		public int compare(Document s1, Document s2) {
			 String titre1 = s1.titre.toUpperCase();
			 String titre2 = s2.titre.toUpperCase();
			 return titre1.compareTo(titre2);
			}
	};

	public static Comparator<Document> DateComparator = new Comparator<Document>() {
		public int compare(Document s1, Document s2) {
			 String date1 = s1.date.toUpperCase();
			 String date2 = s2.date.toUpperCase();
			 return date1.compareTo(date2);
			}
	};
	public static Comparator<Document> NoteComparator = new Comparator<Document>() {
		public int compare(Document s1, Document s2) {
			 String note1 = s1.note.toUpperCase();
			 String note2 = s2.note.toUpperCase();
			 return note1.compareTo(note2);
			}
	};
}
